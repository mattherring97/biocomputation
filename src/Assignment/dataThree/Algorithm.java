/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment.dataThree;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author m2-herring
 */
public class Algorithm {

    public Algorithm() {
    }

    public static String printRulesBitString(Rule[] temp) {
        String s = "";
        for (int i = 0; i < temp.length; i++) {
            s += "Rule " + (i + 1) + ": ";
            for (int j = 0; j < temp[i].condition.length; j++) {
                float a = temp[i].condition[j], b = temp[i].condition[++j];
                if (a < b) {
                    if (a >= 0.4) {
                        s += "1";
                    } else if (b <= 0.6) {
                        s += "0";
                    } else {
                        s += "#";
                    }
                } else {
                    if (b >= 0.4) {
                        s += "1";
                    } else if (a <= 0.6) {
                        s += "0";
                    } else {
                        s += "#";
                    }
                }
            }
            s += " = " + temp[i].out + "\n";
        }
        return s;
    }

    //  Initialse the array and create a new individual
    public static Individual[] initiateArray(int popSize, int geneSize, int ruleNum, int conditionLength) {
        Individual[] temp = new Individual[popSize];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = new Individual(geneSize, ruleNum, conditionLength);
        } //for i
        return temp;
    }

    public static Individual[] createPopulation(Individual[] array) {
        // Create new population and store in array
        // Set the gene size of each individual in the population
        // Populate each gene with floats up to the same length as conditionLength followed by either a 1 or a 0
        int size = array[0].gene.length;
        int conditionLength = array[0].conditionLength;
        for (Individual a : array) {
            for (int j = 1; j < size + 1; j++) {
                if ((j % (conditionLength + 1)) == 0) {
                    a.gene[j - 1] = new Random().nextInt(2);
                } else {
                    float d = (float) Math.random();
                    a.gene[j - 1] = d;
                }
            }
            a.createRulebase(); // Loop through population and convert genes to the rulebases 
        }      
        return array;
    }

    public static Individual[] tournment(Individual[] original) {
        // Tournament selection
        // Selects two random individuals from the population
        // Compares the fitness of the two individuals and stores the fittest as the offspring
        int parent1, parent2;
        Individual[] temp = initiateArray(original.length, original[0].gene.length, original[0].ruleNum, original[0].conditionLength);
        for (int i = 0; i < original.length; i++) {
            parent1 = new Random().nextInt(original.length);
            parent2 = new Random().nextInt(original.length);

            if (original[parent1].fitness >= original[parent2].fitness) {
                temp[i] = new Individual(original[parent1]);
            } else {
                temp[i] = new Individual(original[parent2]);
            }
        }
        return temp;
    }

    public static Individual[] crossover(Individual[] original) {
        // Crossover
        // Selects pairs of genes from the list of offspring
        // Picks a random point in the string and then swaps everything after that point
        // eg. 000|010  == 000|101
        //     111|101  == 111|010
        int geneSize = original[0].gene.length;
        int popSize = original.length;
        int ruleNum = original[0].rulebase.length;
        int conditionLength = original[0].rulebase[0].condition.length;
        Individual[] modified = new Individual[popSize];

        for (int i = 0; i < popSize; i += 2) {
            Individual temp1 = new Individual(geneSize, ruleNum, conditionLength);
            Individual temp2 = new Individual(geneSize, ruleNum, conditionLength);
            int x_point = new Random().nextInt(geneSize);

            for (int j = 0; j < x_point; j++) {
                temp1.gene[j] = original[i].gene[j];
                temp2.gene[j] = original[i + 1].gene[j];
            }

            for (int j = x_point; j < geneSize; j++) {
                temp1.gene[j] = original[i + 1].gene[j];
                temp2.gene[j] = original[i].gene[j];
            }

            temp1.createRulebase();
            temp2.createRulebase();
            modified[i] = new Individual(temp1);
            modified[i + 1] = new Individual(temp2);
        }
        return modified;
    }

    public static Individual[] mutation(Individual[] original, double mutationRate, float mutationSize) {
        // Mutation
        // Traverse the offspring population
        // Perform bitwise mutation at a mutation rate of 1/geneSize      
        int popSize = original.length; // Total size of population (amount of solutions)
        int geneSize = original[0].gene.length; // Total length of each gene (solution)  

        for (int i = 0; i < popSize; i++) { // Loop over each solution in population
            for (int j = 1; j < geneSize + 1; j++) { // Loop over each value in the gene
                double d = Math.random(); // Give each solution a number between 0.0 - 1.0
                if (d < mutationRate) {   
                    if ((j % 13) == 0) {
                        if (original[i].gene[j - 1] == (float) 0.0) {
                            original[i].gene[j - 1] = (float) 1.0;
                        } else {
                            original[i].gene[j - 1] = (float) 0.0;
                        }
                    } else {
                        int operand_selection = new Random().nextInt(2);
                        if (operand_selection == 0) {
                            original[i].gene[j - 1] += mutationSize;
                            if (original[i].gene[j - 1] > 1.0) {
                                original[i].gene[j - 1] = (float) 1.0;
                            }
                        } else {
                            original[i].gene[j - 1] -= mutationSize;
                            if (original[i].gene[j - 1] < 0.0) {
                                original[i].gene[j - 1] = (float) 0.0;
                            }
                        }
                    }
                }
            }
            original[i].createRulebase();
        }
        return original;
    }

    public static void scoreFitness(Individual solution, ArrayList<Data> data) {       
        solution.fitness = 0;
        for (int i = 0; i < data.size(); i++) {
            for (Rule rulebase : solution.rulebase) {
                if (matchesCondition(data.get(i).variables, rulebase.condition) == true) {
                    if (rulebase.out == data.get(i).type) {
                        solution.fitness++;
                    }
                    break;
                }
            }
        }
    }

    public static boolean matchesCondition(float[] data, float[] rule) {
        int k = 0;
        for (int i = 0; i < data.length; i++) {
            if (rule[k] > rule[k + 1]) {
                if ((data[i] > rule[k]) || (rule[k + 1] > data[i])) {
                    return false;
                }
            } else {
                if ((data[i] < rule[k]) || (rule[k + 1] < data[i])) {
                    return false;
                }
            }
            k += 2;
        }
        return true;
    }

    public static Individual evaluate(Individual[] array, Individual best) {
        // Compares each individual in the offspring population to the current fittest individual
        // If the fitness of the current individual being evaluated is > the current fittest individual
        // Set the fittest individual to the current individual and discard the old fittest individual
        Individual temp;
        for (Individual array1 : array) {
            if (array1.fitness > best.fitness) {
                best = array1;
            }
        }
        temp = new Individual(best);
        return temp;
    }

    public static String printRules(Rule[] rules) {
        String s = "";
        int count = 1;
        for (Rule r : rules) {
            s = s + "Rule " + count + ": ";
            for (int i = 0; i < r.condition.length; i++) {
                s = s + "(" + r.condition[i++] + " , " + r.condition[i] + ")  ";
            }
            s = s + " = " + r.out + "\n";
            count++;
        }
        return s;
    }

}
