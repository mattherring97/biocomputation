/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment.dataThree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author m2-herring
 */
public class GA {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String csv = "";
        // Add the filename which must be full file path e.g. "/Users/mherring/NetBeansProjects/src/Assignment/dataThree/data3.txt"
        String filename = "/Users/mherring/NetBeansProjects/src/Assignment/dataThree/data3.txt";

        // Add data objects from file into ArrayLists for testing and training
        ArrayList<Data> dataSet = createDataSet(filename);
        ArrayList<Data> trainingSet = new ArrayList<>();
        ArrayList<Data> testSet = new ArrayList<>();

        // Divide the input file into training and test sets
        for (Data t : dataSet) {
            double d = Math.random();
            if (d < 0.75) {
                trainingSet.add(t);
            } else {
                testSet.add(t);
            }
        }

        // Settings for running the Algorithm 
        int ruleNum = 5; // Number of rules
        int conditionLength = dataSet.get(0).Vars * 2; // Condition length
        int popSize = 100; // Size of the population
        int itteration = 100; // Number of generations
        int multiGA = 100; // How many GAs will run at the same time
        int geneSize = (conditionLength + 1) * ruleNum; // Size of gene per solution
        double mutationRate = 0.02;// Equal to 2% chance of mutation
        float mutationSize = (float) 0.5;// Sets the size of the mutation

        Individual globalBest = new Individual(geneSize, ruleNum, conditionLength); // Store the current best solution
        Individual[] population = Algorithm.initiateArray(popSize, geneSize, ruleNum, conditionLength); // Store the entire population
        Individual[] offspring = Algorithm.initiateArray(popSize, geneSize, ruleNum, conditionLength); // STore the current offspring

        int index = 0;
        while (index < multiGA) { // Set index to 0 and start running GAs at the same time
            Individual best = new Individual(geneSize, ruleNum, conditionLength); // Store the current best solution

            // Create the initial population using random genes and assess fitness
            population = Algorithm.createPopulation(population);
            for (Individual pop : population) {
                Algorithm.scoreFitness(pop, trainingSet);
            }

            int generation = 0;
            while (generation < itteration) {

                // Create offspring by using tournament selection
                offspring = Algorithm.tournment(population);
                for (Individual pop : offspring) {
                    Algorithm.scoreFitness(pop, trainingSet);
                }

                // Perform crossover
                offspring = Algorithm.crossover(offspring);
                for (Individual pop : offspring) {
                    Algorithm.scoreFitness(pop, trainingSet);
                }

                // Perform mutation 
                offspring = Algorithm.mutation(offspring, mutationRate, mutationSize);
                for (Individual pop : offspring) {
                    Algorithm.scoreFitness(pop, trainingSet);
                }

                // Evalutate current offspring against the current best solution
                best = Algorithm.evaluate(offspring, best);

                // Replace current population with offspring and assess fitness
                for (int i = 0; i < popSize; i++) {
                    population[i] = new Individual(offspring[i]);
                }

                generation++;
                csv += best.fitness + ",";
            }
            csv += "\n";

            index++;
            // Check the current best solution against the previous best solution
            if (best.fitness > globalBest.fitness) {
                globalBest = new Individual(best);
            }
        }

        // Print out the results of each GA
        System.out.println("Trained using " + trainingSet.size() + " sets of data");
        System.out.println("Best fitness on the training set " + globalBest.fitness);
        System.out.println(Algorithm.printRules(globalBest.rulebase));
        System.out.println(Algorithm.printRulesBitString(globalBest.rulebase));
        System.out.println(csv);
        double percentTraining = ((double) 100 / trainingSet.size()) * globalBest.fitness;

        Algorithm.scoreFitness(globalBest, testSet);
        System.out.println("Tested using " + testSet.size() + " pieces of data");
        System.out.println("Best fitness over test set " + globalBest.fitness);
        System.out.format("Equals %.2f%% accuracy on training set\n", percentTraining);
        double percentTest = ((double) 100 / testSet.size()) * globalBest.fitness;
        System.out.format("Equals %.2f%% accuracy on test set\n", percentTest);
    }

    public static ArrayList<String> fileToStringArray(String filename) throws FileNotFoundException, IOException {
        ArrayList<String> array = new ArrayList<>();
        // Read the Data file and put contents into a single string
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        reader.readLine();
        String line1 = null;
        while ((line1 = reader.readLine()) != null) {
            String s = "";
            for (int i = 0; i < line1.length(); i++) {
                if ((line1.charAt(i) != '\n')) {
                    s = s + line1.charAt(i);
                }
            }
            array.add(s);
        }
        return array;
    }

    // Read the data from the file to an ArrayList and pass back the ArrayList 
    public static ArrayList<Data> createDataSet(String filename) throws IOException {
        ArrayList<String> fileArray = fileToStringArray(filename);
        ArrayList<Data> tempA = new ArrayList<>();

        for (String a : fileArray) {
            ArrayList<Float> temp = new ArrayList<>();
            Scanner scanner = new Scanner(a);
            scanner.useDelimiter(" ");
            while (scanner.hasNext()) {
                temp.add(scanner.nextFloat());
            }

            Data dataTemp = new Data(temp.size() - 1);
            for (int i = 0; i <= dataTemp.Vars - 1; i++) {
                dataTemp.variables[i] = temp.get(i);
            }
            dataTemp.type = Character.getNumericValue(a.charAt(a.length() - 1));
            tempA.add(dataTemp);
        }
        return tempA;
    }

}
