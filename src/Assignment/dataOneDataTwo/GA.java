/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment.dataOneDataTwo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author m2-herring
 */
public class GA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String csv = "";
        // Add the filename which must be full file path e.g. "/Users/mherring/NetBeansProjects/src/Assignment/dataOneDataTwo/data2.txt"
        String filename = "/Users/mherring/NetBeansProjects/src/Assignment/dataOneDataTwo/data2.txt";

        // Add data objects from file into an ArrayList
        ArrayList<Data> dataSet = createDataSet(filename);

        int ruleNum = 5; // Number of rules
        int conditionLength = dataSet.get(0).Vars; // Condition length
        int popSize = 100; // Size of the population
        int itteration = 500; // Number of generations
        int multiGA = 100; // How many GAs will run at the same time
        int correct_rules = 0; // Sets initial number of correct rules to 0
        int geneSize = (conditionLength + 1) * ruleNum; // Size of gene per solution
        double mutationRate = 0.02; // Equal to 2% chance of mutation

        Individual global_best = new Individual(geneSize, ruleNum, conditionLength); // Store the current best solution
        Individual[] population = Algorithm.initiateArray(popSize, geneSize, ruleNum, conditionLength); // Store the entire population
        Individual[] offspring = Algorithm.initiateArray(popSize, geneSize, ruleNum, conditionLength); // Store the current offspring

        int index = 0;
        while (index < multiGA) { // Set index to 0 and start running GAs at the same time
            Individual best = new Individual(geneSize, ruleNum, conditionLength); // Store the current best solution

            // Create the initial population using random genes
            population = Algorithm.createPopulation(population);

            for (Individual pop : population) {
                Algorithm.scoreFitness(pop, dataSet);
            }

            int generation = 0;
            while (generation < itteration) {

                // Create offspring by using tournament selection
                offspring = Algorithm.tournament(population);
                for (Individual pop : offspring) {
                    Algorithm.scoreFitness(pop, dataSet);
                }

                // Perform crossover
                offspring = Algorithm.crossover(offspring);
                for (Individual pop : offspring) {
                    Algorithm.scoreFitness(pop, dataSet);
                }

                // Perform mutation 
                offspring = Algorithm.mutation(offspring, mutationRate);
                for (Individual pop : offspring) {
                    Algorithm.scoreFitness(pop, dataSet);
                }

                // Evalutate current offspring against the current best solution
                best = Algorithm.evaluate(offspring, best);

                // Replace current population with offspring and assess fitness
                for (int i = 0; i < popSize; i++) {
                    population[i] = new Individual(offspring[i]);
                }

                generation++;
                csv += best.fitness + ",";
            }
            csv += "\n";
            
            if (best.fitness == 64) {
                correct_rules++;
            }
            index++;
            // Check the current best solution against the previous best solution
            if (best.fitness > global_best.fitness) {
                global_best = new Individual(best);
            }
        }
        System.out.println("After " + multiGA + " GAs, " + correct_rules + " correctly identified all " + dataSet.size() + " items in the data set.");
        System.out.println(Algorithm.printRules(global_best.rulebase));
        System.out.println(csv);
    }

    public static ArrayList<String> fileToStringArray(String filename) throws FileNotFoundException, IOException {
        ArrayList<String> array = new ArrayList<>();
        // Read the Data file and put contents into a single string
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        reader.readLine();
        String line1 = null;
        while ((line1 = reader.readLine()) != null) {
            String s = "";
            for (int i = 0; i < line1.length(); i++) {
                if ((line1.charAt(i) == '0') || (line1.charAt(i) == '1')) {
                    s = s + line1.charAt(i);
                }
            }
            array.add(s);
        }
        return array;
    }

    // Read the data from the file to an ArrayList and pass back the ArrayList 
    public static ArrayList<Data> createDataSet(String filename) throws IOException {
        ArrayList<String> fileArray = fileToStringArray(filename);
        int dataSize = fileArray.get(0).length() - 1;
        int k = 0;
        ArrayList<Data> tempA = new ArrayList<>();

        for (String a : fileArray) {
            Data temp = new Data(dataSize);
            for (int i = 0; i < dataSize; i++) {
                temp.variables[i] = Character.getNumericValue(a.charAt(i));
            }
            temp.type = Character.getNumericValue(a.charAt(dataSize));
            tempA.add(temp);
        }
        return tempA;
    }

}
