/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment.dataOneDataTwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author m2-herring
 */
public class Algorithm {

    public Algorithm() {
    }

    // Initialse the array and create a new individual
    public static Individual[] initiateArray(int popSize, int geneSize, int ruleNum, int conditionLength) {
        Individual[] temp = new Individual[popSize];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = new Individual(geneSize, ruleNum, conditionLength);
        }
        return temp;
    }

    public static Individual[] createPopulation(Individual[] array) {
        // Create new population and store in array
        // Set the gene size of each individual in the population
        // Populate each gene with either a 1, 0 or # (wildcard)
        for (Individual a : array) {
            for (int j = 0; j < a.gene.length; j++) {
                ArrayList<String> operators = new ArrayList(Arrays.asList("0", "1", "#"));
                a.gene[j] = operators.get(new Random().nextInt(2));
            }            
            a.createRulebase(); // Loop through population and convert genes to the rulebases 
        }        
        return array;
    }

    public static Individual[] tournament(Individual[] original) {
        // Tournament selection
        // Selects two random individuals from the population
        // Compares the fitness of the two individuals and stores the fittest as the offspring
        int parent1, parent2;
        Individual[] temp = initiateArray(original.length, original[0].gene.length, original[0].ruleNum, original[0].conditionLength);
        for (int i = 0; i < original.length; i++) {
            parent1 = new Random().nextInt(original.length);
            parent2 = new Random().nextInt(original.length);

            if (original[parent1].fitness >= original[parent2].fitness) {
                temp[i] = new Individual(original[parent1]);
            } else {
                temp[i] = new Individual(original[parent2]);
            }
        }
        return temp;
    }

    public static Individual[] crossover(Individual[] original) {
        // Crossover
        // Selects pairs of genes from the list of offspring
        // Picks a random point in the string and then swaps everything after that point
        // eg. 000|010  == 000|101
        //     111|101  == 111|010
        int geneSize = original[0].gene.length;
        int popSize = original.length;
        int ruleNum = original[0].rulebase.length;
        int conditionLength = original[0].rulebase[0].condition.length;
        Individual[] modified = new Individual[popSize];

        for (int i = 0; i < popSize; i += 2) {
            Individual temp1 = new Individual(geneSize, ruleNum, conditionLength);
            Individual temp2 = new Individual(geneSize, ruleNum, conditionLength);
            int x_point = new Random().nextInt(geneSize);

            for (int j = 0; j < x_point; j++) {
                temp1.gene[j] = original[i].gene[j];
                temp2.gene[j] = original[i + 1].gene[j];
            }

            for (int j = x_point; j < geneSize; j++) {
                temp1.gene[j] = original[i + 1].gene[j];
                temp2.gene[j] = original[i].gene[j];
            }

            temp1.createRulebase();
            temp2.createRulebase();
            modified[i] = new Individual(temp1);
            modified[i + 1] = new Individual(temp2);
        }
        return modified;
    }

    public static Individual[] mutation(Individual[] original, double mutationRate) {
        // Mutation
        // Traverse the offspring population
        // Perform bitwise mutation at a mutation rate of 1/geneSize
        int popSize = original.length; // Total size of population (amount of solutions)
        int geneSize = original[0].gene.length; // Total length of each gene (solution)         

        for (int i = 0; i < popSize; i++) { // Loop over each solution in population
            for (int j = 0; j < geneSize; j++) { // Loop over each value in the gene
                double d = Math.random(); // Give each solution a number between 0.0 - 1.0
                if (d < mutationRate) {
                    ArrayList<String> operators = new ArrayList(Arrays.asList("0", "1", "#"));
                    d = Math.random();
                    for (int k = 0; k < operators.size(); k++) {
                        if (original[i].gene[j].equals(operators.get(k))) {
                            String s = operators.get(k);
                            operators.remove(k);
                            int temp;
                            if (d < 0.5) {
                                temp = 0;
                            } else {
                                temp = 1;
                            }
                            original[i].gene[j] = operators.get(new Random().nextInt(2));
                            System.out.print("");
                            operators.add(s);
                        }
                    }
                }
            }
            original[i].createRulebase();
        }
        return original;
    }

    public static Individual evaluate(Individual[] array, Individual best) {
        // Compares each individual in the offspring population to the current fittest individual
        // If the fitness of the current individual being evaluated is > the current fittest individual
        // Set the fittest individual to the current individual and discard the old fittest individual
        Individual temp;
        for (Individual array1 : array) {
            if (array1.fitness > best.fitness) {
                best = array1;
            }
        }
        temp = new Individual(best);
        return temp;
    }

    public static String printRules(Rule[] rules) {
        String s = "";
        int count = 1;
        for (Rule r : rules) {
            s = s + "Rule " + count + ": ";
            for (int i = 0; i < r.condition.length; i++) {
                s = s + r.condition[i];
            }
            s = s + " = " + r.out + "\n";
            count++;
        }
        return s;
    }

    public static boolean matchesCondition(int[] data, String[] rule) {
        for (int i = 0; i < data.length; i++) {
            String s = "" + data[i];
            if ((rule[i].equals(s) != true) && (rule[i].equals("#") != true)) {
                return false;
            }
        }
        return true;
    }

    public static void scoreFitness(Individual solution, ArrayList<Data> data) {

        solution.fitness = 0;
        for (int i = 0; i < data.size(); i++) {
            for (Rule rulebase : solution.rulebase) {
                if (matchesCondition(data.get(i).variables, rulebase.condition) == true) {
                    String s = "" + data.get(i).type;
                    if (rulebase.out.equals(s) == true) {
                        solution.fitness++;
                    }
                    break;
                }
            }
        }
    }

}
