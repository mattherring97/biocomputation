/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment.dataOneDataTwo;

/**
 *
 * @author m2-herring
 */
public class Individual {

    int ruleNum;
    int conditionLength;
    String[] gene;
    int fitness = 0;
    Rule[] rulebase;

    // Initialise the individual and populate the rulebase
    public Individual(int n, int ruleNum, int conditionLength) {
        this.gene = new String[n];
        this.fitness = 0;
        this.conditionLength = conditionLength;
        this.ruleNum = ruleNum;
        createRulebase();

    }

    // Create a new individual
    public Individual(Individual n) {
        this.conditionLength = n.conditionLength;
        this.ruleNum = n.ruleNum;
        this.gene = n.gene;
        this.fitness = n.fitness;
        this.rulebase = n.rulebase;

    }

    public void getFitness() {
        System.out.println("The fitness is " + fitness);
    }

    // Create the rulebase
    public void createRulebase() {
        int fitness = 0;
        int k = 0;

        this.rulebase = new Rule[ruleNum];
        for (int i = 0; i < ruleNum; i++) {
            rulebase[i] = new Rule(conditionLength);
            for (int j = 0; j < conditionLength; j++) {
                rulebase[i].condition[j] = gene[k++];
            }
            rulebase[i].out = gene[k++];
        }

    }
}
